﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OnlineShop.Helpers;
using OnlineShop.Model;
using OnlineShop.UnitTests.Tests_Helpers;

namespace OnlineShop.UnitTests.Helpers
{
    [TestClass]
    public class DiscountsFitTheOrderTests
    {
        private DiscountsFitTheOrder _discountsFitTheOrder;

        private RandomData _randomData;

        [TestInitialize]
        public void TestInitialize()
        {
            _discountsFitTheOrder = new DiscountsFitTheOrder();

            _randomData = new RandomData();
        }

        #region GetFitQuantityDiscountsForProducts method

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetFitQuantityDiscountsForProducts_NullValueAsParam1_ExceptionThrown()
        {
            _discountsFitTheOrder.GetFitQuantityDiscountsForProducts(null, _randomData.CreateRandomDiscountsProductQuantity());
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetFitQuantityDiscountsForProducts_AtLeastOneProductIsNullInParam1__ExceptionThrown()
        {
            List<ProductOrder> productsOrder = new List<ProductOrder>()
            {
                new ProductOrder() { Product = _randomData.CreateRandomProduct()},
                new ProductOrder() { Product = null},
                new ProductOrder() { Product = _randomData.CreateRandomProduct()},
            };

            _discountsFitTheOrder.GetFitQuantityDiscountsForProducts(productsOrder, _randomData.CreateRandomDiscountsProductQuantity());
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetFitQuantityDiscountsForProducts_AtLeastTwoProductsHaveTheSameIdInParam1_ExceptionThrown()
        {
            List<ProductOrder> productsOrder = new List<ProductOrder>()
            {
                new ProductOrder() { Product = _randomData.CreateRandomProduct()},
                new ProductOrder() { Product = _randomData.CreateRandomProduct()},
                new ProductOrder() { Product = _randomData.CreateRandomProduct()},
            };

            productsOrder[0].Product.Id = 1;
            productsOrder[2].Product.Id = 1;

            _discountsFitTheOrder.GetFitQuantityDiscountsForProducts(productsOrder, _randomData.CreateRandomDiscountsProductQuantity());
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetFitQuantityDiscountsForProducts_NullValueAsParam2_ExceptionThrown()
        {
            _discountsFitTheOrder.GetFitQuantityDiscountsForProducts(_randomData.CreateRandomProductOrderList(), null);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetFitQuantityDiscountsForProducts_AtLeastOneProductIsNullInParam2__ExceptionThrown()
        {
            List<DiscountProductQuantity> discountsProductQuantity = new List<DiscountProductQuantity>()
            {
                new DiscountProductQuantity() { Product = _randomData.CreateRandomProduct()},
                new DiscountProductQuantity() { Product = null},
                new DiscountProductQuantity() { Product = _randomData.CreateRandomProduct()},
            };

            _discountsFitTheOrder.GetFitQuantityDiscountsForProducts(_randomData.CreateRandomProductOrderList(), discountsProductQuantity);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetFitQuantityDiscountsForProducts_AtLeastTwoProductsHaveTheSameIdInParam2_ExceptionThrown()
        {
            List<DiscountProductQuantity> discountsProductQuantity = new List<DiscountProductQuantity>()
            {
                new DiscountProductQuantity() { Product = _randomData.CreateRandomProduct()},
                new DiscountProductQuantity() { Product = _randomData.CreateRandomProduct()},
                new DiscountProductQuantity() { Product = _randomData.CreateRandomProduct()},
            };

            discountsProductQuantity[0].Product.Id = 1;
            discountsProductQuantity[2].Product.Id = 1;

            _discountsFitTheOrder.GetFitQuantityDiscountsForProducts(_randomData.CreateRandomProductOrderList(), discountsProductQuantity);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void GetFitQuantityDiscountsForProducts_AtLeastOneDiscountHaveValueOutsideTheRangeInParam2_ExceptionThrown()
        {
            List<DiscountProductQuantity> discountsProductQuantity = new List<DiscountProductQuantity>()
            {
                new DiscountProductQuantity() { Product = _randomData.CreateRandomProduct(), Discount = 100.00M},
                new DiscountProductQuantity() { Product = _randomData.CreateRandomProduct(), Discount = 67.00M},
                new DiscountProductQuantity() { Product = _randomData.CreateRandomProduct(), Discount = 1.00M}
            };

            _discountsFitTheOrder.GetFitQuantityDiscountsForProducts(_randomData.CreateRandomProductOrderList(), discountsProductQuantity);
        }

        [TestMethod]
        public void GetFitQuantityDiscountsForProducts_SimpleValues_Calculated()
        {
            Product testProduct_1 = _randomData.CreateRandomProduct();
            Product testProduct_2 = _randomData.CreateRandomProduct();
            Product testProduct_3 = _randomData.CreateRandomProduct();

            List<ProductOrder> productsOrder = new List<ProductOrder>()
            {
                new ProductOrder() { Product = testProduct_1, Quantity = 2},
                new ProductOrder() { Product = testProduct_2, Quantity = 4},
                new ProductOrder() { Product = testProduct_3, Quantity = 8}
            };

            List<DiscountProductQuantity> discountsProductQuantity = new List<DiscountProductQuantity>()
            {
                new DiscountProductQuantity() { Product = testProduct_1, QuantityOfProducts = 3},
                new DiscountProductQuantity() { Product = testProduct_2, QuantityOfProducts = 6},
                new DiscountProductQuantity() { Product = testProduct_3, QuantityOfProducts = 2},
            };

            int expected = 1;

            List<DiscountProductQuantity> result =
                _discountsFitTheOrder.GetFitQuantityDiscountsForProducts(productsOrder, discountsProductQuantity);

            Assert.AreEqual(expected, result.Count);
        }

        #endregion GetFitQuantityDiscountsForProducts method

        #region GetFitQuantityDiscountsForProductCategory method

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetFitQuantityDiscountsForProductCategory_NullValueAsParam1_ExceptionThrown()
        {
            _discountsFitTheOrder.GetFitQuantityDiscountsForProductCategory(null, _randomData.CreateRandomDiscountsProductCategoryQuantity());
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetFitQuantityDiscountsForProductCategory_AtLeastOneProductIsNullInParam1__ExceptionThrown()
        {
            List<ProductOrder> productsOrder = new List<ProductOrder>()
            {
                new ProductOrder() { Product = _randomData.CreateRandomProduct()},
                new ProductOrder() { Product = null},
                new ProductOrder() { Product = _randomData.CreateRandomProduct()},
            };

            _discountsFitTheOrder.GetFitQuantityDiscountsForProductCategory(productsOrder, _randomData.CreateRandomDiscountsProductCategoryQuantity());
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetFitQuantityDiscountsForProductCategory_AtLeastTwoProductsHaveTheSameIdInParam1_ExceptionThrown()
        {
            List<ProductOrder> productsOrder = new List<ProductOrder>()
            {
                new ProductOrder() { Product = _randomData.CreateRandomProduct()},
                new ProductOrder() { Product = _randomData.CreateRandomProduct()},
                new ProductOrder() { Product = _randomData.CreateRandomProduct()},
            };

            productsOrder[0].Product.Id = 1;
            productsOrder[2].Product.Id = 1;

            _discountsFitTheOrder.
                GetFitQuantityDiscountsForProductCategory(productsOrder, _randomData.CreateRandomDiscountsProductCategoryQuantity());
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetFitQuantityDiscountsForProductCategory_NullValueAsParam2_ExceptionThrown()
        {
            _discountsFitTheOrder.GetFitQuantityDiscountsForProductCategory(_randomData.CreateRandomProductOrderList(), null);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetFitQuantityDiscountsForProductCategory_AtLeastOneCategoryIsNullInParam2__ExceptionThrown()
        {
            List<DiscountProductCategoryQuantity> discountsProductCategoryQuantity = new List<DiscountProductCategoryQuantity>()
            {
                new DiscountProductCategoryQuantity() { Category = _randomData.CreateRandomCategory()},
                new DiscountProductCategoryQuantity() { Category = null},
                new DiscountProductCategoryQuantity() { Category = _randomData.CreateRandomCategory()},
            };

            _discountsFitTheOrder.GetFitQuantityDiscountsForProductCategory(_randomData.CreateRandomProductOrderList(), discountsProductCategoryQuantity);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetFitQuantityDiscountsForProductCategory_AtLeastTwoCategoriesHaveTheSameIdInParam2_ExceptionThrown()
        {
            List<DiscountProductCategoryQuantity> discountsProductCategoryQuantity = new List<DiscountProductCategoryQuantity>()
            {
                new DiscountProductCategoryQuantity() { Category = _randomData.CreateRandomCategory()},
                new DiscountProductCategoryQuantity() { Category = _randomData.CreateRandomCategory()},
                new DiscountProductCategoryQuantity() { Category = _randomData.CreateRandomCategory()},
            };

            discountsProductCategoryQuantity[0].Category.Id = 1;
            discountsProductCategoryQuantity[2].Category.Id = 1;

            _discountsFitTheOrder.GetFitQuantityDiscountsForProductCategory(_randomData.CreateRandomProductOrderList(), discountsProductCategoryQuantity);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void GetFitQuantityDiscountsForProductCategory_AtLeastOneDiscountHaveValueOutsideTheRangeInParam2_ExceptionThrown()
        {
            List<DiscountProductCategoryQuantity> discountsProductCategoryQuantity = new List<DiscountProductCategoryQuantity>()
            {
                new DiscountProductCategoryQuantity() { Category = _randomData.CreateRandomCategory(), Discount = 34.00M},
                new DiscountProductCategoryQuantity() { Category = _randomData.CreateRandomCategory(), Discount = -1.00M},
                new DiscountProductCategoryQuantity() { Category = _randomData.CreateRandomCategory(), Discount = 1.00M},
            };

            _discountsFitTheOrder.GetFitQuantityDiscountsForProductCategory(_randomData.CreateRandomProductOrderList(), discountsProductCategoryQuantity);
        }

        [TestMethod]
        public void GetFitQuantityDiscountsForProductCategory_SimpleValues_Calculated()
        {
            Category testCategory_1 = _randomData.CreateRandomCategory();
            Category testCategory_2 = _randomData.CreateRandomCategory();
            Category testCategory_3 = _randomData.CreateRandomCategory();

            Product testProduct_1 = _randomData.CreateRandomProduct();
            testProduct_1.Category = testCategory_1;
            Product testProduct_2 = _randomData.CreateRandomProduct();
            testProduct_2.Category = testCategory_1;
            Product testProduct_3 = _randomData.CreateRandomProduct();
            testProduct_3.Category = testCategory_2;

            List<ProductOrder> productsOrder = new List<ProductOrder>()
            {
                new ProductOrder() {Product = testProduct_1, Quantity = 2},
                new ProductOrder() {Product = testProduct_2, Quantity = 4},
                new ProductOrder() {Product = testProduct_3, Quantity = 2}
            };

            List<DiscountProductCategoryQuantity> discountsProductCategoryQuantity = new List<DiscountProductCategoryQuantity>()
            {
                new DiscountProductCategoryQuantity() { Category = testCategory_1, QuantityOfProducts = 3},
                new DiscountProductCategoryQuantity() { Category = testCategory_2, QuantityOfProducts = 6},
                new DiscountProductCategoryQuantity() { Category = testCategory_3, QuantityOfProducts = 4}
            };

            int expected = 1;

            List<DiscountProductCategoryQuantity> result =
                _discountsFitTheOrder.GetFitQuantityDiscountsForProductCategory(productsOrder, discountsProductCategoryQuantity);

            Assert.AreEqual(expected, result.Count);
        }

        #endregion GetFitQuantityDiscountsForProductCategory method
    }
}