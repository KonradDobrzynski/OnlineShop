﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OnlineShop.Helpers;
using OnlineShop.Model;
using OnlineShop.UnitTests.Tests_Helpers;

namespace OnlineShop.UnitTests.Helpers
{
    [TestClass]
    public class CalculateOrderTests
    {
        private CalculateOrder _calculateOrder;

        private RandomData _randomData;

        [TestInitialize]
        public void TestInitialize()
        {
            _calculateOrder = new CalculateOrder();

            _randomData = new RandomData();
        }

        #region GetTotalValueWithoutDiscounts method

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetTotalValueWithoutDiscounts_NullValueAsParam1_ExceptionThrown()
        {
            _calculateOrder.GetTotalValueWithoutDiscounts(null);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetTotalValueWithoutDiscounts_AtLeastOneProductIsNullInParam1__ExceptionThrown()
        {
            List<ProductOrder> productsOrder = new List<ProductOrder>()
            {
                new ProductOrder() { Product = _randomData.CreateRandomProduct()},
                new ProductOrder() { Product = null},
                new ProductOrder() { Product = _randomData.CreateRandomProduct()},
            };

            _calculateOrder.GetTotalValueWithoutDiscounts(productsOrder);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetTotalValueWithoutDiscounts_AtLeastTwoProductsHaveTheSameIdInParam1_ExceptionThrown()
        {
            List<ProductOrder> productsOrder = new List<ProductOrder>()
            {
                new ProductOrder() { Product = _randomData.CreateRandomProduct()},
                new ProductOrder() { Product = _randomData.CreateRandomProduct()},
                new ProductOrder() { Product = _randomData.CreateRandomProduct()},
            };

            productsOrder[0].Product.Id = 1;
            productsOrder[2].Product.Id = 1;

            _calculateOrder.GetTotalValueWithoutDiscounts(productsOrder);
        }

        [TestMethod]
        public void GetTotalValueWithoutDiscounts_SimpleValues_Calculated()
        {
            Product testProduct_1 = _randomData.CreateRandomProduct();
            testProduct_1.NetPrice = 23.00M;
            Product testProduct_2 = _randomData.CreateRandomProduct();
            testProduct_2.NetPrice = 12.50M;
            Product testProduct_3 = _randomData.CreateRandomProduct();
            testProduct_3.NetPrice = 41.45M;

            List<ProductOrder> productsOrder = new List<ProductOrder>()
            {
                new ProductOrder() { Product = testProduct_1, Quantity = 2},
                new ProductOrder() { Product = testProduct_2, Quantity = 4},
                new ProductOrder() { Product = testProduct_3, Quantity = 8}
            };

            decimal expected = 427.60M;

            decimal result = _calculateOrder.GetTotalValueWithoutDiscounts(productsOrder);

            Assert.AreEqual(expected, result);
        }

        #endregion GetTotalValueWithoutDiscounts method

        #region GetTotalValueWithDiscounts method

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetTotalValueWithDiscounts_NullValueAsParam1_ExceptionThrown()
        {
            _calculateOrder.GetTotalValueWithDiscounts(null, _randomData.CreateRandomDiscountsOrder());
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetTotalValueWithDiscounts_AtLeastOneProductIsNullInParam1__ExceptionThrown()
        {
            List<ProductOrder> productsOrder = new List<ProductOrder>()
            {
                new ProductOrder() { Product = _randomData.CreateRandomProduct()},
                new ProductOrder() { Product = null},
                new ProductOrder() { Product = _randomData.CreateRandomProduct()},
            };

            _calculateOrder.GetTotalValueWithDiscounts(productsOrder, _randomData.CreateRandomDiscountsOrder());
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetTotalValueWithDiscounts_AtLeastTwoProductsHaveTheSameIdInParam1_ExceptionThrown()
        {
            List<ProductOrder> productsOrder = new List<ProductOrder>()
            {
                new ProductOrder() { Product = _randomData.CreateRandomProduct()},
                new ProductOrder() { Product = _randomData.CreateRandomProduct()},
                new ProductOrder() { Product = _randomData.CreateRandomProduct()},
            };

            productsOrder[0].Product.Id = 1;
            productsOrder[2].Product.Id = 1;

            _calculateOrder.GetTotalValueWithDiscounts(productsOrder, _randomData.CreateRandomDiscountsOrder());
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetTotalValueWithDiscounts_NullValueAsParam2_ExceptionThrown()
        {
            _calculateOrder.GetTotalValueWithDiscounts(_randomData.CreateRandomProductOrderList(), null);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetTotalValueWithDiscounts_DiscountProductQuantityListAsNullInParam2_ExceptionThrown()
        {
            DiscountsOrder discountsOrder = _randomData.CreateRandomDiscountsOrder();
            discountsOrder.DiscountProductQuantity = null;

            _calculateOrder.GetTotalValueWithDiscounts(_randomData.CreateRandomProductOrderList(), discountsOrder);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetTotalValueWithDiscounts_AtLeastOneProductInDiscountProductQuantityListIsNullInParam2__ExceptionThrown()
        {
            List<DiscountProductQuantity> discountsProductQuantity = new List<DiscountProductQuantity>()
            {
                new DiscountProductQuantity() { Product = _randomData.CreateRandomProduct()},
                new DiscountProductQuantity() { Product = null},
                new DiscountProductQuantity() { Product = _randomData.CreateRandomProduct()},
            };

            DiscountsOrder discountsOrder = _randomData.CreateRandomDiscountsOrder();
            discountsOrder.DiscountProductQuantity = discountsProductQuantity;

            _calculateOrder.GetTotalValueWithDiscounts(_randomData.CreateRandomProductOrderList(), discountsOrder);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetTotalValueWithDiscounts_AtLeastTwoProductsHaveTheSameIdInParam2_ExceptionThrown()
        {
            List<DiscountProductQuantity> discountsProductQuantity = new List<DiscountProductQuantity>()
            {
                new DiscountProductQuantity() { Product = _randomData.CreateRandomProduct()},
                new DiscountProductQuantity() { Product = _randomData.CreateRandomProduct()},
                new DiscountProductQuantity() { Product = _randomData.CreateRandomProduct()},
            };

            discountsProductQuantity[0].Product.Id = 1;
            discountsProductQuantity[2].Product.Id = 1;

            DiscountsOrder discountsOrder = _randomData.CreateRandomDiscountsOrder();
            discountsOrder.DiscountProductQuantity = discountsProductQuantity;

            _calculateOrder.GetTotalValueWithDiscounts(_randomData.CreateRandomProductOrderList(), discountsOrder);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetTotalValueWithDiscounts_DiscountProductCategoryQuantityListAsNullInParam2_ExceptionThrown()
        {
            DiscountsOrder discountsOrder = _randomData.CreateRandomDiscountsOrder();
            discountsOrder.DiscountProductCategoryQuantity = null;

            _calculateOrder.GetTotalValueWithDiscounts(_randomData.CreateRandomProductOrderList(), discountsOrder);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetTotalValueWithDiscounts_AtLeastOneCategoryInDiscountProductQuantityListIsNullInParam2__ExceptionThrown()
        {
            List<DiscountProductCategoryQuantity> discountsProductCategoryQuantity = new List<DiscountProductCategoryQuantity>()
            {
                new DiscountProductCategoryQuantity() { Category = _randomData.CreateRandomCategory()},
                new DiscountProductCategoryQuantity() { Category = null},
                new DiscountProductCategoryQuantity() { Category = _randomData.CreateRandomCategory()},
            };

            DiscountsOrder discountsOrder = _randomData.CreateRandomDiscountsOrder();
            discountsOrder.DiscountProductCategoryQuantity = discountsProductCategoryQuantity;

            _calculateOrder.GetTotalValueWithDiscounts(_randomData.CreateRandomProductOrderList(), discountsOrder);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetTotalValueWithDiscounts_AtLeastTwoCategoriesHaveTheSameIdInParam2_ExceptionThrown()
        {
            List<DiscountProductCategoryQuantity> discountsProductCategoryQuantity = new List<DiscountProductCategoryQuantity>()
            {
                new DiscountProductCategoryQuantity() { Category = _randomData.CreateRandomCategory()},
                new DiscountProductCategoryQuantity() { Category = _randomData.CreateRandomCategory()},
                new DiscountProductCategoryQuantity() { Category = _randomData.CreateRandomCategory()},
            };

            discountsProductCategoryQuantity[0].Category.Id = 1;
            discountsProductCategoryQuantity[2].Category.Id = 1;

            DiscountsOrder discountsOrder = _randomData.CreateRandomDiscountsOrder();
            discountsOrder.DiscountProductCategoryQuantity = discountsProductCategoryQuantity;

            _calculateOrder.GetTotalValueWithDiscounts(_randomData.CreateRandomProductOrderList(), discountsOrder);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void GetTotalValueWithDiscounts_AtLeastOneDiscountProductQuantityHaveValueOutsideTheRangeInParam2_ExceptionThrown()
        {
            DiscountsOrder discountsOrder = _randomData.CreateRandomDiscountsOrder();

            discountsOrder.DiscountProductQuantity.Add(new DiscountProductQuantity()
            {
                Product = _randomData.CreateRandomProduct(),
                Discount = 99.99M
            });

            _calculateOrder.GetTotalValueWithDiscounts(_randomData.CreateRandomProductOrderList(), discountsOrder);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void GetTotalValueWithDiscounts_AtLeastOneDiscountProductCategoryQuantityHaveValueOutsideTheRangeInParam2_ExceptionThrown()
        {
            DiscountsOrder discountsOrder = _randomData.CreateRandomDiscountsOrder();

            discountsOrder.DiscountProductCategoryQuantity.Add(new DiscountProductCategoryQuantity()
            {
                Category = _randomData.CreateRandomCategory(),
                Discount = 100.00M
            });

            _calculateOrder.GetTotalValueWithDiscounts(_randomData.CreateRandomProductOrderList(), discountsOrder);
        }

        [TestMethod]
        public void GetTotalValueWithDiscounts_SimpleValuesWithTrueValueAsParam3_Calculated()
        {
            Category testCategory_1 = _randomData.CreateRandomCategory();
            Category testCategory_2 = _randomData.CreateRandomCategory();

            Product testProduct_1 = _randomData.CreateRandomProduct();
            testProduct_1.NetPrice = 2000.00M;
            testProduct_1.Category = testCategory_1;
            Product testProduct_2 = _randomData.CreateRandomProduct();
            testProduct_2.NetPrice = 130.00M;
            testProduct_2.Category = testCategory_1;
            Product testProduct_3 = _randomData.CreateRandomProduct();
            testProduct_3.NetPrice = 30.00M;
            testProduct_3.Category = testCategory_1;
            Product testProduct_4 = _randomData.CreateRandomProduct();
            testProduct_4.NetPrice = 1500.00M;
            testProduct_4.Category = testCategory_1;
            Product testProduct_5 = _randomData.CreateRandomProduct();
            testProduct_5.NetPrice = 45.00M;
            testProduct_5.Category = testCategory_2;
            Product testProduct_6 = _randomData.CreateRandomProduct();
            testProduct_6.NetPrice = 550.00M;
            testProduct_6.Category = testCategory_2;

            List<ProductOrder> productsOrder = new List<ProductOrder>()
            {
                new ProductOrder() { Product = testProduct_1, Quantity = 1},
                new ProductOrder() { Product = testProduct_2, Quantity = 2},
                new ProductOrder() { Product = testProduct_3, Quantity = 1},
                new ProductOrder() { Product = testProduct_4, Quantity = 3},
                new ProductOrder() { Product = testProduct_5, Quantity = 1},
                new ProductOrder() { Product = testProduct_6, Quantity = 3}
            };

            List<DiscountProductQuantity> discountsProductQuantity = new List<DiscountProductQuantity>()
            {
                new DiscountProductQuantity() { Product = testProduct_4, Discount = 10, QuantityOfProducts = 3},
                new DiscountProductQuantity() { Product = testProduct_6, Discount = 40, QuantityOfProducts = 1}
            };

            List<DiscountProductCategoryQuantity> discountsProductCategoryQuantity = new List<DiscountProductCategoryQuantity>()
            {
                new DiscountProductCategoryQuantity() { Category = testCategory_1, Discount = 25, QuantityOfProducts = 6},
                new DiscountProductCategoryQuantity() { Category = testCategory_2, Discount = 60, QuantityOfProducts = 2},
            };

            DiscountsOrder discountsOrder = new DiscountsOrder();
            discountsOrder.DiscountProductQuantity = discountsProductQuantity;
            discountsOrder.DiscountProductCategoryQuantity = discountsProductCategoryQuantity;

            decimal expected = 2212.50M;

            decimal result = _calculateOrder.GetTotalValueWithDiscounts(productsOrder, discountsOrder, true);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void GetTotalValueWithDiscounts_SimpleValuesWithFalseValueAsParam3_Calculated()
        {
            Category testCategory_1 = _randomData.CreateRandomCategory();
            Category testCategory_2 = _randomData.CreateRandomCategory();
            Category testCategory_3 = _randomData.CreateRandomCategory();
            Category testCategory_4 = _randomData.CreateRandomCategory();
            Category testCategory_5 = _randomData.CreateRandomCategory();

            Product testProduct_1 = _randomData.CreateRandomProduct();
            testProduct_1.NetPrice = 2000.00M;
            testProduct_1.Category = testCategory_1;
            Product testProduct_2 = _randomData.CreateRandomProduct();
            testProduct_2.NetPrice = 130.00M;
            testProduct_2.Category = testCategory_1;
            Product testProduct_3 = _randomData.CreateRandomProduct();
            testProduct_3.NetPrice = 30.00M;
            testProduct_3.Category = testCategory_1;
            Product testProduct_4 = _randomData.CreateRandomProduct();
            testProduct_4.NetPrice = 1500.00M;
            testProduct_4.Category = testCategory_1;
            Product testProduct_5 = _randomData.CreateRandomProduct();
            testProduct_5.NetPrice = 45.00M;
            testProduct_5.Category = testCategory_2;
            Product testProduct_6 = _randomData.CreateRandomProduct();
            testProduct_6.NetPrice = 550.00M;
            testProduct_6.Category = testCategory_2;

            List<ProductOrder> productsOrder = new List<ProductOrder>()
            {
                new ProductOrder() { Product = testProduct_1, Quantity = 1},
                new ProductOrder() { Product = testProduct_2, Quantity = 2},
                new ProductOrder() { Product = testProduct_3, Quantity = 1},
                new ProductOrder() { Product = testProduct_4, Quantity = 3},
                new ProductOrder() { Product = testProduct_5, Quantity = 1},
                new ProductOrder() { Product = testProduct_6, Quantity = 3}
            };

            List<DiscountProductQuantity> discountsProductQuantity = new List<DiscountProductQuantity>()
            {
                new DiscountProductQuantity() { Product = testProduct_4, Discount = 10, QuantityOfProducts = 3},
                new DiscountProductQuantity() { Product = testProduct_6, Discount = 40, QuantityOfProducts = 1},
                new DiscountProductQuantity() { Product = testProduct_1, Discount = 10, QuantityOfProducts = 15},
                new DiscountProductQuantity() { Product = testProduct_2, Discount = 23, QuantityOfProducts = 7},
                new DiscountProductQuantity() { Product = testProduct_3, Discount = 23, QuantityOfProducts = 9},
                new DiscountProductQuantity() { Product = testProduct_5, Discount = 45, QuantityOfProducts = 12}
            };

            List<DiscountProductCategoryQuantity> discountsProductCategoryQuantity = new List<DiscountProductCategoryQuantity>()
            {
                new DiscountProductCategoryQuantity() { Category = testCategory_1, Discount = 25, QuantityOfProducts = 6},
                new DiscountProductCategoryQuantity() { Category = testCategory_2, Discount = 60, QuantityOfProducts = 2},
                new DiscountProductCategoryQuantity() { Category = testCategory_3, Discount = 33, QuantityOfProducts = 6},
                new DiscountProductCategoryQuantity() { Category = testCategory_4, Discount = 4, QuantityOfProducts = 2},
                new DiscountProductCategoryQuantity() { Category = testCategory_5, Discount = 12, QuantityOfProducts = 4}
            };

            DiscountsOrder discountsOrder = new DiscountsOrder();
            discountsOrder.DiscountProductQuantity = discountsProductQuantity;
            discountsOrder.DiscountProductCategoryQuantity = discountsProductCategoryQuantity;

            decimal expected = 2212.50M;

            decimal result = _calculateOrder.GetTotalValueWithDiscounts(productsOrder, discountsOrder);

            Assert.AreEqual(expected, result);
        }

        #endregion GetTotalValueWithDiscounts method
    }
}