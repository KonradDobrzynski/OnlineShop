﻿using System;
using System.Collections.Generic;
using OnlineShop.Model;

namespace OnlineShop.UnitTests.Tests_Helpers
{
    public class RandomData
    {
        private readonly Random _random;

        private int _productCounter;

        private int _categoryCounter;

        public RandomData()
        {
            _random = new Random();
        }

        public Product CreateRandomProduct()
        {
            return new Product()
            {
                Id = _productCounter++,
                CreateDate = DateTime.Now,
                Name = "Product",
                NetPrice = 15.00M,
                Category = CreateRandomCategory()
            };
        }

        public Category CreateRandomCategory()
        {
            return new Category()
            {
                Id = _categoryCounter++,
                CreateDate = DateTime.Now,
                Name = "Category"
            };
        }

        public List<ProductOrder> CreateRandomProductOrderList()
        {
            return new List<ProductOrder>()
            {
                new ProductOrder() {Product = CreateRandomProduct()},
                new ProductOrder() {Product = CreateRandomProduct()}
            };
        }

        public List<DiscountProductQuantity> CreateRandomDiscountsProductQuantity()
        {
            return new List<DiscountProductQuantity>()
            {
                new DiscountProductQuantity()
                {
                    Product = CreateRandomProduct(),
                    Discount = _random.Next(0, 99),
                    QuantityOfProducts = _random.Next(0, 100)
                },
                new DiscountProductQuantity()
                {
                    Product = CreateRandomProduct(),
                    Discount = _random.Next(0, 99),
                    QuantityOfProducts = _random.Next(0, 100)
                }
            };
        }

        public List<DiscountProductCategoryQuantity> CreateRandomDiscountsProductCategoryQuantity()
        {
            return new List<DiscountProductCategoryQuantity>()
            {
                new DiscountProductCategoryQuantity()
                {
                    Category = CreateRandomCategory(),
                    Discount = _random.Next(0, 99),
                    QuantityOfProducts = _random.Next(0, 100)
                },
                new DiscountProductCategoryQuantity()
                {
                    Category = CreateRandomCategory(),
                    Discount = _random.Next(0, 99),
                    QuantityOfProducts = _random.Next(0, 100)
                }
            };
        }

        public DiscountsOrder CreateRandomDiscountsOrder()
        {
            return new DiscountsOrder()
            {
                DiscountProductQuantity = CreateRandomDiscountsProductQuantity(),
                DiscountProductCategoryQuantity = CreateRandomDiscountsProductCategoryQuantity()
            };
        }
    }
}