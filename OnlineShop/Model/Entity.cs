﻿using System;

namespace OnlineShop.Model
{
    /// <summary>Basic class for all models.</summary>
    public abstract class Entity<T>
    {
        /// <summary>Id object in database.</summary>
        public T Id { get; set; }

        /// <summary>Object deleted in database.</summary>
        public bool Deleted { get; set; }

        /// <summary>Object hidden in database.</summary>
        public bool Hidden { get; set; }

        /// <summary>Creation date object.</summary>
        public DateTime CreateDate { get; set; }
    }
}