﻿namespace OnlineShop.Model
{
    /// <summary>Category model for products in online shop.</summary>
    public class Category : Entity<int>
    {
        /// <summary>Category name.</summary>
        public string Name { get; set; }
    }
}