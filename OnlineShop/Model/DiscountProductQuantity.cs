﻿namespace OnlineShop.Model
{
    /// <summary>Class defining discount by individual item.</summary>
    public class DiscountProductQuantity : DiscountBase
    {
        /// <summary>Product associated with the discount.</summary>
        public Product Product { get; set; }
    }
}