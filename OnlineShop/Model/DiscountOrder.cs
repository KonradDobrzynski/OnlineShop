﻿using System.Collections.Generic;

namespace OnlineShop.Model
{
    /// <summary>Class defining discounts in the order.</summary>
    public class DiscountsOrder
    {
        public DiscountsOrder()
        {
            DiscountProductQuantity = new List<DiscountProductQuantity>();

            DiscountProductCategoryQuantity = new List<DiscountProductCategoryQuantity>();
        }

        /// <summary>List of discounts for individual item associated with the order.</summary>
        public List<DiscountProductQuantity> DiscountProductQuantity { get; set; }

        /// <summary>List of discounts for all items based on category products associated with the order.</summary>
        public List<DiscountProductCategoryQuantity> DiscountProductCategoryQuantity { get; set; }
    }
}