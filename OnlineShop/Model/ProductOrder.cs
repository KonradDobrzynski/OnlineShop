﻿namespace OnlineShop.Model
{
    /// <summary>Class defining products in the order.</summary>
    public class ProductOrder : Entity<int>
    {
        /// <summary>Product quantity.</summary>
        public int Quantity { get; set; }

        /// <summary>Product in order.</summary>
        public Product Product { get; set; }
    }
}