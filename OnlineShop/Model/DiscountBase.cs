﻿namespace OnlineShop.Model
{
    /// <summary>Base class for all discounts.</summary>
    public abstract class DiscountBase : Entity<int>
    {
        /// <summary>Name discount.</summary>
        public string Name { get; set; }

        /// <summary>Discount in percentage.</summary>
        public decimal Discount { get; set; }

        /// <summary>Number of products to activate discount.</summary>
        public int QuantityOfProducts { get; set; }
    }
}