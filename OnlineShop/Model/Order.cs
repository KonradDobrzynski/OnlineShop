﻿using System.Collections.Generic;

namespace OnlineShop.Model
{
    /// <summary>Order model in online shop.</summary>
    public class Order : Entity<int>
    {
        public Order()
        {
            ProductOrders = new List<ProductOrder>();

            DiscountsOrder = new DiscountsOrder();
        }

        /// <summary>Order name.</summary>
        public string Name { get; set; }

        /// <summary>Discounts existing in online store and associated with the order.</summary>
        public DiscountsOrder DiscountsOrder { get; set; }

        /// <summary>Products associated with the order and their quantity.</summary>
        public List<ProductOrder> ProductOrders { get; set; }

        /// <summary>Price without discounts.</summary>
        public decimal TotalValueWithoutDiscounts { get; set; }

        /// <summary>Price with discounts.</summary>
        public decimal TotalValueWithDiscounts { get; set; }
    }
}