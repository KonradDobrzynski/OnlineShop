﻿namespace OnlineShop.Model
{
    /// <summary>Product model in online shop.</summary>
    public class Product : Entity<int>
    {
        /// <summary>Product name.</summary>
        public string Name { get; set; }

        /// <summary>Product category.</summary>
        public Category Category { get; set; }

        private decimal _netPrice;

        /// <summary>Product net price.</summary>
        public decimal NetPrice
        {
            get
            {
                return _netPrice;
            }
            set
            {
                _netPrice = decimal.Round(value, 2);
            }
        }
    }
}