﻿namespace OnlineShop.Model
{
    /// <summary>Class defining discount by category.</summary>
    public class DiscountProductCategoryQuantity : DiscountBase
    {
        /// <summary>Category associated with the discount.</summary>
        public Category Category { get; set; }
    }
}