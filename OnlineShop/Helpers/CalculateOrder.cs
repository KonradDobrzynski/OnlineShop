﻿using System.Collections.Generic;
using System.Linq;
using OnlineShop.Model;
using OnlineShop.Validators;

namespace OnlineShop.Helpers
{
    /// <summary>
    /// A class that allows to calculate the final price for the order based on the products and discounts defined in the order.
    /// </summary>
    public class CalculateOrder
    {
        private readonly OrderValidator _orderValidator;

        public CalculateOrder()
        {
            _orderValidator = new OrderValidator();
        }

        #region Public methods

        /// <summary>The method returns the final price without discounts.</summary>
        /// <param name="productOrders"> Products in Order.</param>
        public decimal GetTotalValueWithoutDiscounts(List<ProductOrder> productOrders)
        {
            _orderValidator.ValidateProductsOrder(productOrders);

            return productOrders.Sum(x => x.Product.NetPrice * x.Quantity);
        }

        /// <summary>The method returns the final price with discounts.</summary>
        /// <param name="productOrders"> Products in Order.</param>
        /// <param name="discountsOrder"> Discounts in Order.</param>
        /// <param name="discountFitToOrder"> Discounts fit to Order.</param>
        public decimal GetTotalValueWithDiscounts(List<ProductOrder> productOrders, DiscountsOrder discountsOrder, bool discountFitToOrder = false)
        {
            ValidationParameters(productOrders, discountsOrder);

            DiscountsOrder discounts;

            discounts = discountFitToOrder
                ? discountsOrder
                : FittingDiscountsToOrder(discountsOrder, productOrders);

            decimal totalPrice = CalculateQuantityDiscountsForProducts(productOrders, discounts.DiscountProductQuantity);

            totalPrice = CalculateQuantityDiscountsForCategory(discounts.DiscountProductCategoryQuantity,
                totalPrice);

            return totalPrice;
        }

        #endregion Public methods

        #region Helpers

        private DiscountsOrder FittingDiscountsToOrder(DiscountsOrder discountsOrder, List<ProductOrder> productOrders)
        {
            DiscountsOrder filteredDiscounts = discountsOrder;

            DiscountsFitTheOrder discountsFitTheOrder = new DiscountsFitTheOrder();

            discountsOrder.DiscountProductQuantity = discountsFitTheOrder.GetFitQuantityDiscountsForProducts(productOrders,
                discountsOrder.DiscountProductQuantity);

            discountsOrder.DiscountProductCategoryQuantity = discountsFitTheOrder.GetFitQuantityDiscountsForProductCategory(productOrders,
                    discountsOrder.DiscountProductCategoryQuantity);

            return filteredDiscounts;
        }

        private void ValidationParameters(List<ProductOrder> productOrders, DiscountsOrder discountsOrder)
        {
            _orderValidator.ValidateProductsOrder(productOrders);

            _orderValidator.ValidateAllDiscounts(discountsOrder);
        }

        private decimal CalculateQuantityDiscountsForProducts(List<ProductOrder> productOrders, List<DiscountProductQuantity> discountsProductQuantity)
        {
            decimal totalPriceForIndividualProducts = 0;

            foreach (var productOrder in productOrders)
            {
                var discount = discountsProductQuantity.
                    FirstOrDefault(
                        x =>
                            x.Product.Id == productOrder.Product.Id &&
                            productOrder.Quantity >= x.QuantityOfProducts);

                if (discount == null)
                {
                    totalPriceForIndividualProducts += productOrder.Product.NetPrice * productOrder.Quantity;
                }
                else
                {
                    decimal priceAfterDiscount = decimal.Round(productOrder.Product.NetPrice * 0.01M * (100 - discount.Discount), 2);

                    if (priceAfterDiscount == 0)
                    {
                        priceAfterDiscount = 0.01M;
                    }

                    totalPriceForIndividualProducts += priceAfterDiscount * productOrder.Quantity;
                }
            }

            return totalPriceForIndividualProducts;
        }

        private decimal CalculateQuantityDiscountsForCategory(List<DiscountProductCategoryQuantity> discountsProductCategoryQuantity, decimal totalPriceAfterQuantityDiscountsForProducts)
        {
            decimal totalPriceWithDiscountsCategory = totalPriceAfterQuantityDiscountsForProducts;

            foreach (var discount in discountsProductCategoryQuantity)
            {
                totalPriceWithDiscountsCategory =
                    decimal.Round(totalPriceWithDiscountsCategory * 0.01M * (100 - discount.Discount), 2);
            }

            return totalPriceWithDiscountsCategory;
        }

        #endregion Helpers
    }
}