﻿using System.Collections.Generic;
using System.Linq;
using OnlineShop.Model;
using OnlineShop.Validators;

namespace OnlineShop.Helpers
{
    public class DiscountsFitTheOrder
    {
        private readonly OrderValidator _orderValidator;

        public DiscountsFitTheOrder()
        {
            _orderValidator = new OrderValidator();
        }

        #region Public methods

        /// <summary>The method returns filtered quantity discounts for products.</summary>
        /// <param name="productOrders">Products in Order.</param>
        /// <param name="discountsProductQuantity">Discounts for filtering.</param>
        public List<DiscountProductQuantity> GetFitQuantityDiscountsForProducts(List<ProductOrder> productOrders, List<DiscountProductQuantity> discountsProductQuantity)
        {
            _orderValidator.ValidateProductsOrder(productOrders);

            _orderValidator.ValidateQuantityDiscountsForProducts(discountsProductQuantity);

            return discountsProductQuantity.
                Where(x => productOrders.Any(y => y.Product.Id == x.Product.Id &&
                y.Quantity >= x.QuantityOfProducts)).ToList();
        }

        /// <summary>The method returns filtered quantity discounts for categories</summary>
        /// <param name="productOrders">Products in Order.</param>
        /// <param name="discountsProductCategoryQuantity">Discounts for filtering.</param>
        public List<DiscountProductCategoryQuantity> GetFitQuantityDiscountsForProductCategory(List<ProductOrder> productOrders, List<DiscountProductCategoryQuantity> discountsProductCategoryQuantity)
        {
            _orderValidator.ValidateProductsOrder(productOrders);

            _orderValidator.ValidateQuantityDiscountsForCategories(discountsProductCategoryQuantity);

            var quantityOfProductsByCategory = productOrders.GroupBy(x => x.Product.Category.Id).Select(x => new
            {
                categoryId = x.Key,
                quantityProducts = x.Sum(y => y.Quantity),
            });

            return discountsProductCategoryQuantity.
                Where(x => quantityOfProductsByCategory.Any(y => y.categoryId == x.Category.Id &&
                y.quantityProducts >= x.QuantityOfProducts)).ToList();
        }

        #endregion Public methods
    }
}