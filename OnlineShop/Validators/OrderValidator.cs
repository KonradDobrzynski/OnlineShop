﻿using System;
using System.Collections.Generic;
using System.Linq;
using OnlineShop.Model;

namespace OnlineShop.Validators
{
    internal class OrderValidator
    {
        #region Public methods

        /// <summary>The method validate products in order.</summary>
        /// <param name="productOrders">Products in order to validation.</param>
        public void ValidateProductsOrder(List<ProductOrder> productOrders)
        {
            string productOrdersType = typeof(List<ProductOrder>).ToString();

            if (productOrders == null)
            {
                throw new Exception($"List can not be null. Type list: {productOrdersType}");
            }

            foreach (var productOrder in productOrders)
            {
                if (productOrder.Product == null)
                {
                    throw new Exception($"At least one item in the list contains null product. Type list: {productOrdersType}");
                }
            }

            if (productOrders.GroupBy(x => x.Product.Id).Any(y => y.Count() > 1))
            {
                throw new Exception($"The list contains at least one repetition of the product with the same Id. Type list: {productOrdersType}");
            }
        }

        /// <summary>The method validate all discounts in order.</summary>
        /// <param name="discountsOrder">Discounts in order to validation.</param>
        public void ValidateAllDiscounts(DiscountsOrder discountsOrder)
        {
            string discountsType = typeof(DiscountsOrder).ToString();

            if (discountsOrder == null)
            {
                throw new Exception($"Object can not be null. Type object: {discountsType}");
            }

            ValidateQuantityDiscountsForProducts(discountsOrder.DiscountProductQuantity);

            ValidateQuantityDiscountsForCategories(discountsOrder.DiscountProductCategoryQuantity);
        }

        /// <summary>The method validate discounts for products.</summary>
        /// <param name="discountsProductQuantity">Discounts.</param>
        public void ValidateQuantityDiscountsForProducts(List<DiscountProductQuantity> discountsProductQuantity)
        {
            string listType = typeof(List<DiscountProductQuantity>).ToString();

            if (discountsProductQuantity == null)
            {
                throw new Exception($"List can not be null. Type list: {listType}");
            }

            foreach (var discountQuantity in discountsProductQuantity)
            {
                if (discountQuantity.Product == null)
                {
                    throw new Exception($"At least one item in the list contains null product. Type list: {listType}");
                }

                ValidateDiscountRange(discountQuantity.Discount);
            }

            if (discountsProductQuantity.GroupBy(x => x.Product.Id).Any(y => y.Count() > 1))
            {
                throw new Exception("$The list includes at least two discounts for the same product. Type list: {typeList}");
            }
        }

        /// <summary>The method validate discounts for categories.</summary>
        /// <param name="discountsProductCategoryQuantity">Discounts.</param>
        public void ValidateQuantityDiscountsForCategories(List<DiscountProductCategoryQuantity> discountsProductCategoryQuantity)
        {
            string listType = typeof(List<DiscountProductCategoryQuantity>).ToString();

            if (discountsProductCategoryQuantity == null)
            {
                throw new Exception($"List can not be null. Type list: {listType}");
            }

            foreach (var discountCategory in discountsProductCategoryQuantity)
            {
                if (discountCategory.Category == null)
                {
                    throw new Exception($"At least one item in the list contains null category. Type list: {listType}");
                }

                ValidateDiscountRange(discountCategory.Discount);
            }

            if (discountsProductCategoryQuantity.GroupBy(x => x.Category.Id).Any(y => y.Count() > 1))
            {
                throw new Exception($"The list includes at least two discounts for the same category. Type list: {listType}");
            }
        }

        #endregion Public methods

        #region Private methods

        private void ValidateDiscountRange(decimal discount)
        {
            if (!(discount >= 0 && discount <= 99))
            {
                throw new ArgumentOutOfRangeException(nameof(discount), discount, "Discount must be between 0 and 99 inclusive");
            }
        }

        #endregion Private methods
    }
}