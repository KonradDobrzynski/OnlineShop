﻿using System.Collections.Generic;
using OnlineShop.Model;

namespace Sample.Model
{
    public class DataFromDatabase
    {
        public DataFromDatabase()
        {
            Categories = new List<Category>();

            Products = new List<Product>();

            DiscountQuantities = new List<DiscountProductQuantity>();

            DiscountCategories = new List<DiscountProductCategoryQuantity>();
        }

        public List<Category> Categories { get; set; }

        public List<Product> Products { get; set; }

        public List<DiscountProductQuantity> DiscountQuantities { get; set; }

        public List<DiscountProductCategoryQuantity> DiscountCategories { get; set; }
    }
}