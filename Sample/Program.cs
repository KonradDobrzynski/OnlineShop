﻿using System;
using System.Collections.Generic;
using System.Linq;
using OnlineShop.Helpers;
using OnlineShop.Model;
using Sample.Database;
using Sample.Model;
using Sample.ViewModel;

namespace Sample
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            // Create pseudo data from "database"  (data are eg. repository or interface)
            PseudoDatabase pseudoDatabase = new PseudoDatabase();
            DataFromDatabase dataFromDatabase = pseudoDatabase.CreateSimpleData();

            List<Category> categories = dataFromDatabase.Categories;
            List<Product> products = dataFromDatabase.Products;
            List<DiscountProductQuantity> discountQuantities = dataFromDatabase.DiscountQuantities;
            List<DiscountProductCategoryQuantity> discountCategories = dataFromDatabase.DiscountCategories;

            // ViewModel that comes eg. from a view in method parameters (in ASP.NET MVC Application)
            OrderViewModel orderViewModel = new OrderViewModel();
            orderViewModel.ProductOrderViewModels.Add(new ProductOrderViewModel { ProductId = 1, Quantity = 1 });
            orderViewModel.ProductOrderViewModels.Add(new ProductOrderViewModel { ProductId = 4, Quantity = 2 });
            orderViewModel.ProductOrderViewModels.Add(new ProductOrderViewModel { ProductId = 5, Quantity = 1 });
            orderViewModel.ProductOrderViewModels.Add(new ProductOrderViewModel { ProductId = 2, Quantity = 3 });
            orderViewModel.ProductOrderViewModels.Add(new ProductOrderViewModel { ProductId = 6, Quantity = 1 });
            orderViewModel.ProductOrderViewModels.Add(new ProductOrderViewModel { ProductId = 7, Quantity = 3 });

            // Simulation "AddOrder" POST method in Controller
            Order order = new Order();
            order.Name = "634 / 07 / 2017";

            foreach (var productOrderViewModel in orderViewModel.ProductOrderViewModels)
                order.ProductOrders.Add(new ProductOrder
                {
                    Product = products.FirstOrDefault(x => x.Id == productOrderViewModel.ProductId),
                    Quantity = productOrderViewModel.Quantity
                });

            DiscountsFitTheOrder discountsFitTheOrder = new DiscountsFitTheOrder();

            order.DiscountsOrder.DiscountProductQuantity = discountsFitTheOrder.GetFitQuantityDiscountsForProducts(order.ProductOrders, discountQuantities);

            order.DiscountsOrder.DiscountProductCategoryQuantity = discountsFitTheOrder.GetFitQuantityDiscountsForProductCategory(order.ProductOrders, discountCategories);

            // Calculating Order
            var calculateOrder = new CalculateOrder();

            order.TotalValueWithoutDiscounts = calculateOrder.GetTotalValueWithoutDiscounts(order.ProductOrders);

            DiscountsOrder discountsOrder = new DiscountsOrder();
            discountsOrder.DiscountProductCategoryQuantity = discountCategories;
            discountsOrder.DiscountProductQuantity = discountQuantities;

            order.TotalValueWithDiscounts =
                calculateOrder.GetTotalValueWithDiscounts(order.ProductOrders, discountsOrder, false);

            // Save order in database and redirect to view.
            // ...

            Console.WriteLine($"Total value without discounts: {order.TotalValueWithoutDiscounts}");
            Console.WriteLine("\n");
            Console.WriteLine($"Total value with discounts: {order.TotalValueWithDiscounts}");
            Console.ReadKey();
        }
    }
}