﻿using System.Collections.Generic;

namespace Sample.ViewModel
{
    public class OrderViewModel
    {
        public OrderViewModel()
        {
            ProductOrderViewModels = new List<ProductOrderViewModel>();
        }

        public List<ProductOrderViewModel> ProductOrderViewModels { get; set; }
    }
}