﻿namespace Sample.ViewModel
{
    public class ProductOrderViewModel
    {
        public int Quantity { get; set; }

        public int ProductId { get; set; }
    }
}
