﻿using OnlineShop.Model;
using Sample.Model;

namespace Sample.Database
{
    public class PseudoDatabase
    {
        public DataFromDatabase CreateSimpleData()
        {
            DataFromDatabase dataFromDatabase = new DataFromDatabase();

            #region Create categories

            Category electronicsCategory = new Category() { Id = 1, Name = "Electronics" };
            dataFromDatabase.Categories.Add(electronicsCategory);

            Category toolsCategory = new Category() { Id = 2, Name = "Tools" };
            dataFromDatabase.Categories.Add(toolsCategory);

            Category booksCategory = new Category() { Id = 3, Name = "Books" };
            dataFromDatabase.Categories.Add(booksCategory);

            #endregion Create categories

            #region Create products

            Product computerProduct = new Product()
            {
                Id = 1,
                Name = "Computer",
                Category = electronicsCategory,
                NetPrice = (decimal)2000.00
            };
            dataFromDatabase.Products.Add(computerProduct);

            Product phoneProduct = new Product()
            {
                Id = 2,
                Name = "Phone",
                Category = electronicsCategory,
                NetPrice = (decimal)1500.00
            };
            dataFromDatabase.Products.Add(phoneProduct);

            Product speakersProduct = new Product()
            {
                Id = 3,
                Name = "Speakers",
                Category = electronicsCategory,
                NetPrice = (decimal)450.50
            };
            dataFromDatabase.Products.Add(speakersProduct);

            Product mouseProduct = new Product()
            {
                Id = 4,
                Name = "Mouse",
                Category = electronicsCategory,
                NetPrice = (decimal)130.00
            };
            dataFromDatabase.Products.Add(mouseProduct);

            Product usbCableProduct = new Product()
            {
                Id = 5,
                Name = "UsbCable",
                Category = electronicsCategory,
                NetPrice = (decimal)30.00
            };
            dataFromDatabase.Products.Add(usbCableProduct);

            Product hammerProduct = new Product()
            {
                Id = 6,
                Name = "Hammer",
                Category = toolsCategory,
                NetPrice = (decimal)45.00
            };
            dataFromDatabase.Products.Add(hammerProduct);

            Product drillProduct = new Product()
            {
                Id = 7,
                Name = "Drill",
                Category = toolsCategory,
                NetPrice = (decimal)550.00
            };
            dataFromDatabase.Products.Add(drillProduct);

            Product rakeProduct = new Product()
            {
                Id = 8,
                Name = "Rake",
                Category = toolsCategory,
                NetPrice = (decimal)90.00
            };
            dataFromDatabase.Products.Add(rakeProduct);

            #endregion Create products

            #region Create discounts

            DiscountProductQuantity discountQuantity_1 = new DiscountProductQuantity()
            {
                Id = 1,
                Name = "Phone discount 10%",
                Discount = 10,
                Product = phoneProduct,
                QuantityOfProducts = 3
            };
            dataFromDatabase.DiscountQuantities.Add(discountQuantity_1);

            DiscountProductQuantity discountQuantity_2 = new DiscountProductQuantity()
            {
                Id = 2,
                Name = "Drill discount 40%",
                Discount = 40,
                Product = drillProduct,
                QuantityOfProducts = 1
            };
            dataFromDatabase.DiscountQuantities.Add(discountQuantity_2);

            DiscountProductQuantity discountQuantity_3 = new DiscountProductQuantity()
            {
                Id = 3,
                Name = "Computer discount 40%",
                Discount = 40,
                Product = computerProduct,
                QuantityOfProducts = 2
            };
            dataFromDatabase.DiscountQuantities.Add(discountQuantity_3);

            DiscountProductQuantity discountQuantity_4 = new DiscountProductQuantity()
            {
                Id = 2,
                Name = "Rake discount 35%",
                Discount = 35,
                Product = rakeProduct,
                QuantityOfProducts = 1
            };
            dataFromDatabase.DiscountQuantities.Add(discountQuantity_4);

            DiscountProductCategoryQuantity discountCategoryProductQuantity1 = new DiscountProductCategoryQuantity()
            {
                Id = 1,
                Name = "Tools category discount 25%",
                Discount = 25,
                Category = toolsCategory,
                QuantityOfProducts = 2
            };
            dataFromDatabase.DiscountCategories.Add(discountCategoryProductQuantity1);

            DiscountProductCategoryQuantity discountCategoryProductQuantity_2 = new DiscountProductCategoryQuantity()
            {
                Id = 2,
                Name = "Electronics category discount 60%",
                Discount = 60,
                Category = electronicsCategory,
                QuantityOfProducts = 6
            };
            dataFromDatabase.DiscountCategories.Add(discountCategoryProductQuantity_2);

            DiscountProductCategoryQuantity discountCategoryProductQuantity_3 = new DiscountProductCategoryQuantity()
            {
                Id = 2,
                Name = "Books category discount 25%",
                Discount = 25,
                Category = booksCategory,
                QuantityOfProducts = 6
            };
            dataFromDatabase.DiscountCategories.Add(discountCategoryProductQuantity_3);

            #endregion Create discounts

            return dataFromDatabase;
        }
    }
}